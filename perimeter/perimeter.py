"Модуль."


def calc_triangle_perimeter(
    side_a: int | float, side_b: int | float, side_c: int | float
) -> int | float:
    """
    Расчет периметра треугольника.

    Arguments:
        side_a -- Сторона A
        side_b -- Сторона B
        side_c -- Сторона C

    Returns:
        Периметр
    """
    if side_a < 0 or side_b < 0 or side_c < 0:
        return -1
    return sum([side_a, side_b, side_c])
