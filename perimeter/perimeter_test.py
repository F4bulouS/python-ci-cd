"Тест расчета периметра."


import unittest

from perimeter.perimeter import calc_triangle_perimeter


class TestStringMethods(unittest.TestCase):
    """Test string."""

    def test_calc_triangle_perimeter_success(self):
        """Test string."""
        self.assertEqual(calc_triangle_perimeter(1, 2, 2), 5)

    def test_calc_triangle_perimeter_fail_bad_side(self):
        """Test string."""
        self.assertEqual(calc_triangle_perimeter(-3, 4, 5), -1)
